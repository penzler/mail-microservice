package com.example.mailclient.service;

import java.util.ArrayList;

import com.example.mailclient.model.DAOUser;
import com.example.mailclient.model.UserDTO;
import com.example.mailclient.model.UserDTOUpdateRequest;
import com.example.mailclient.repository.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserDAO userDao;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        DAOUser user = userDao.findByName(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        return new org.springframework.security.core.userdetails.User(user.getName(), user.getPassword(),
                new ArrayList<>());
    }

    public DAOUser save(UserDTO user) {
        System.out.println("Creating new admin: " + user.getUsername());
        DAOUser newUser = new DAOUser();
        newUser.setName(user.getUsername());
        newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
        return userDao.save(newUser);
    }

    public DAOUser saveGlobalAdmin(UserDTO user) {
        System.out.println("Creating new global admin: " + user.getUsername());
        DAOUser newUser = new DAOUser();
        newUser.setGlobalAdmin(true);
        newUser.setName(user.getUsername());
        newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
        return userDao.save(newUser);
    }

    public DAOUser update(UserDTOUpdateRequest userDTOUpdateRequest, Integer userId) {
        DAOUser existingUser = userDao.findByName(userDTOUpdateRequest.getUsername());
        if (existingUser != null) {
            System.out.println("Updating existing user: " + existingUser.getName());
            if (userDTOUpdateRequest.getPassword() != null) {
                existingUser.setPassword(bcryptEncoder.encode(userDTOUpdateRequest.getPassword()));
            }
            return userDao.save(existingUser);
        }
        throw new UsernameNotFoundException("User not found with userId: " + userId);
    }
}
