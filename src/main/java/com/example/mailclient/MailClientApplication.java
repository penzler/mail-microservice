package com.example.mailclient;

import com.example.mailclient.model.DAOUser;
import com.example.mailclient.model.UserDTO;
import com.example.mailclient.repository.UserDAO;
import com.example.mailclient.service.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
@EnableAutoConfiguration
public class MailClientApplication extends SpringBootServletInitializer implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(MailClientApplication.class, args);
	}
	@Autowired
	private JwtUserDetailsService userDetailsService;

	@Autowired
	private UserDAO userDAO;

	@Override
	public void run(String... args) throws Exception {
		// INSERT DEMO ADMIN USER IF NOT EXISTS
		DAOUser user1 = userDAO.findByName("erik");
		if (user1 == null) {
			UserDTO initialAdminUser = new UserDTO("erik", "erik");
			userDetailsService.save(initialAdminUser);
		}
		// INSERT DEMO GLOBAL ADMIN USER IF NOT EXISTS
		DAOUser user2 = userDAO.findByName("david");
		if (user2 == null) {
			UserDTO initialGlobalAdminUser = new UserDTO("david", "david");
			userDetailsService.saveGlobalAdmin(initialGlobalAdminUser);
		}
		// IMPORT ALL DATA FROM DEMO.sql file

	}
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**")
						.allowedMethods("HEAD", "GET", "PUT", "POST", "DELETE", "PATCH");
			}
		};
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(MailClientApplication.class);
	}
}
