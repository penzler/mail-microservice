package com.example.mailclient.model;

import lombok.Data;
import org.apache.commons.lang3.RandomStringUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

@Data
@Entity
public class Session implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String uniqueId;

    @NotNull
    private String emailAdress;


    public Session(@NotNull String emailAdress) {
        this.emailAdress = emailAdress;
        this.uniqueId = RandomStringUtils.randomAlphanumeric(10);;
    }

    public Session() {
    }

    public Long getId() {
        return id;
    }

    public String getUnique_id() {
        return uniqueId;
    }

    public String getEmailAdress() {
        return emailAdress;
    }

    @Override
    public String toString() {
        return "Session{" +
                "id=" + id +
                ", uniqueId='" + uniqueId + '\'' +
                ", emailAdress='" + emailAdress + '\'' +
                '}';
    }
}
