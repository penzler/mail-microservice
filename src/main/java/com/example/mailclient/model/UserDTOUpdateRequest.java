package com.example.mailclient.model;

import java.io.Serializable;

public class UserDTOUpdateRequest implements Serializable {
    private String username;
    private String password;
    private int existingId;

    public UserDTOUpdateRequest(){

    }

    public UserDTOUpdateRequest(String username, String password, int existingId) {
        this.username = username;
        this.password = password;
        this.existingId = existingId;
    }

    public int getExistingId() {
        return existingId;
    }

    public void setExistingId(int existingId) {
        this.existingId = existingId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
