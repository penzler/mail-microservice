package com.example.mailclient.model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;


public class SaveLivestreams implements Serializable {
    List<String> livestreams;
    String username;

    public SaveLivestreams() {
    }

    public SaveLivestreams(List<String> livestreams, String username) {
        this.livestreams = livestreams;
        this.username = username;
    }

    public List<String> getLivestreams() {
        return livestreams;
    }

    public void setLivestreams(List<String> livestreams) {
        this.livestreams = livestreams;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SaveLivestreams that = (SaveLivestreams) o;
        return Objects.equals(livestreams, that.livestreams) &&
                Objects.equals(username, that.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(livestreams, username);
    }
}
