package com.example.mailclient.model;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "livestream", schema = "test", catalog = "")
public class LivestreamEntity implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int idInstitute;
    private int idCategory;
    private int idUser;
    private String idDevice;
    private String tmsId;
    private String tmsRecordedUri;
    private Timestamp startDate;
    private Timestamp endDate;
    private byte hidden;
    private byte access;
    @ColumnDefault("0")
    private byte comments;
    private String rtmpPath;
    @ColumnDefault("\"sinput/default.jpg\"")
    private String thumbPath;

    private String name;
    @Column(name = "\"desc\"")
    private String desc;
    private String link;
    private int views;
    private BigDecimal rating;
    private int promo;
    private byte live;
    private byte expired;
    private Integer conferenceId;
    private String password;

    @ManyToMany(mappedBy = "livestreamEntities")
    @Transient
    private Set<DAOUser> users = new HashSet<>();

    public Set<DAOUser> getUsers() {
        return users;
    }

    public void setUsers(Set<DAOUser> users) {
        this.users = users;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_institute")
    public int getIdInstitute() {
        return idInstitute;
    }

    public void setIdInstitute(int idInstitute) {
        this.idInstitute = idInstitute;
    }

    @Basic
    @Column(name = "id_category")
    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    @Basic
    @Column(name = "id_user")
    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    @Basic
    @Column(name = "id_device")
    public String getIdDevice() {
        return idDevice;
    }

    public void setIdDevice(String idDevice) {
        this.idDevice = idDevice;
    }

    @Basic
    @Column(name = "tms_id")
    public String getTmsId() {
        return tmsId;
    }

    public void setTmsId(String tmsId) {
        this.tmsId = tmsId;
    }

    @Basic
    @Column(name = "tms_recorded_uri")
    public String getTmsRecordedUri() {
        return tmsRecordedUri;
    }

    public void setTmsRecordedUri(String tmsRecordedUri) {
        this.tmsRecordedUri = tmsRecordedUri;
    }

    @Basic
    @Column(name = "start_date")
    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "end_date")
    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    @Basic
    @Column(name = "hidden")
    public byte getHidden() {
        return hidden;
    }

    public void setHidden(byte hidden) {
        this.hidden = hidden;
    }

    @Basic
    @Column(name = "access")
    public byte getAccess() {
        return access;
    }

    public void setAccess(byte access) {
        this.access = access;
    }

    @Basic
    @Column(name = "comments")
    public byte getComments() {
        return comments;
    }

    public void setComments(byte comments) {
        this.comments = comments;
    }

    @Basic
    @Column(name = "rtmp_path")
    public String getRtmpPath() {
        return rtmpPath;
    }

    public void setRtmpPath(String rtmpPath) {
        this.rtmpPath = rtmpPath;
    }

    @Basic
    @Column(name = "thumb_path")
    public String getThumbPath() {
        return thumbPath;
    }

    public void setThumbPath(String thumbPath) {
        this.thumbPath = thumbPath;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "\"desc\"")
    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Basic
    @Column(name = "link")
    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Basic
    @Column(name = "views")
    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    @Basic
    @Column(name = "rating")
    public BigDecimal getRating() {
        return rating;
    }

    public void setRating(BigDecimal rating) {
        this.rating = rating;
    }

    @Basic
    @Column(name = "promo")
    public int getPromo() {
        return promo;
    }

    public void setPromo(int promo) {
        this.promo = promo;
    }

    @Basic
    @Column(name = "live")
    public byte getLive() {
        return live;
    }

    public void setLive(byte live) {
        this.live = live;
    }

    @Basic
    @Column(name = "expired")
    public byte getExpired() {
        return expired;
    }

    public void setExpired(byte expired) {
        this.expired = expired;
    }

    @Basic
    @Column(name = "conference_id")
    public Integer getConferenceId() {
        return conferenceId;
    }

    public void setConferenceId(Integer conferenceId) {
        this.conferenceId = conferenceId;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LivestreamEntity that = (LivestreamEntity) o;

        if (id != that.id) return false;
        if (idInstitute != that.idInstitute) return false;
        if (idCategory != that.idCategory) return false;
        if (idUser != that.idUser) return false;
        if (hidden != that.hidden) return false;
        if (access != that.access) return false;
        if (comments != that.comments) return false;
        if (views != that.views) return false;
        if (promo != that.promo) return false;
        if (live != that.live) return false;
        if (expired != that.expired) return false;
        if (idDevice != null ? !idDevice.equals(that.idDevice) : that.idDevice != null) return false;
        if (tmsId != null ? !tmsId.equals(that.tmsId) : that.tmsId != null) return false;
        if (tmsRecordedUri != null ? !tmsRecordedUri.equals(that.tmsRecordedUri) : that.tmsRecordedUri != null)
            return false;
        if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;
        if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null) return false;
        if (rtmpPath != null ? !rtmpPath.equals(that.rtmpPath) : that.rtmpPath != null) return false;
        if (thumbPath != null ? !thumbPath.equals(that.thumbPath) : that.thumbPath != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (desc != null ? !desc.equals(that.desc) : that.desc != null) return false;
        if (link != null ? !link.equals(that.link) : that.link != null) return false;
        if (rating != null ? !rating.equals(that.rating) : that.rating != null) return false;
        if (conferenceId != null ? !conferenceId.equals(that.conferenceId) : that.conferenceId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idInstitute;
        result = 31 * result + idCategory;
        result = 31 * result + idUser;
        result = 31 * result + (idDevice != null ? idDevice.hashCode() : 0);
        result = 31 * result + (tmsId != null ? tmsId.hashCode() : 0);
        result = 31 * result + (tmsRecordedUri != null ? tmsRecordedUri.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (int) hidden;
        result = 31 * result + (int) access;
        result = 31 * result + (int) comments;
        result = 31 * result + (rtmpPath != null ? rtmpPath.hashCode() : 0);
        result = 31 * result + (thumbPath != null ? thumbPath.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (desc != null ? desc.hashCode() : 0);
        result = 31 * result + (link != null ? link.hashCode() : 0);
        result = 31 * result + views;
        result = 31 * result + (rating != null ? rating.hashCode() : 0);
        result = 31 * result + promo;
        result = 31 * result + (int) live;
        result = 31 * result + (int) expired;
        result = 31 * result + (conferenceId != null ? conferenceId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LivestreamEntity{" +
                "id=" + id +
                ", idInstitute=" + idInstitute +
                ", idCategory=" + idCategory +
                ", idUser=" + idUser +
                ", idDevice='" + idDevice + '\'' +
                ", tmsId='" + tmsId + '\'' +
                ", tmsRecordedUri='" + tmsRecordedUri + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", hidden=" + hidden +
                ", access=" + access +
                ", comments=" + comments +
                ", rtmpPath='" + rtmpPath + '\'' +
                ", thumbPath='" + thumbPath + '\'' +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                ", link='" + link + '\'' +
                ", views=" + views +
                ", rating=" + rating +
                ", promo=" + promo +
                ", live=" + live +
                ", expired=" + expired +
                ", conferenceId=" + conferenceId +
                ", password='" + password + '\'' +
                ", users=" + users +
                '}';
    }
}