package com.example.mailclient.repository;

import com.example.mailclient.model.DAOUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;


@Repository
public interface UserDAO extends JpaRepository<DAOUser, Integer> {
    DAOUser findByName(String name);
    DAOUser findById(Set<DAOUser> id);


}
