package com.example.mailclient.repository;

import com.example.mailclient.model.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface SessionRepository extends JpaRepository<Session, Integer> {
   Session findByEmailAdress(String emailAdress);
   Session findByUniqueId(String id);
}
