package com.example.mailclient.controller;


import java.util.Objects;

import com.example.mailclient.config.JwtRequest;
import com.example.mailclient.config.JwtResponse;
import com.example.mailclient.config.JwtTokenUtil;
import com.example.mailclient.model.DAOUser;
import com.example.mailclient.model.UserDTO;
import com.example.mailclient.repository.UserDAO;
import com.example.mailclient.service.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin(origins = "*")
public class JwtAuthenticationController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private JwtUserDetailsService userDetailsService;
    @Autowired
    private UserDAO userDAO;

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());
        final String token = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new JwtResponse(token));
    }

    @RequestMapping(value = "/authenticate/globaladmin", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationTokenGlobalAdmin(@RequestBody JwtRequest authenticationRequest) throws Exception {
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());
        final DAOUser user = userDAO.findByName(authenticationRequest.getUsername());
        if (user.isGlobalAdmin()){
            final String token = jwtTokenUtil.generateToken(userDetails);
            return ResponseEntity.ok(new JwtResponse(token));
        }
        return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }


    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> saveUser(@RequestBody UserDTO proposedAdminUser, @RequestHeader HttpHeaders headers) throws Exception {
        if (!Objects.requireNonNull(headers.get("Authorization")).isEmpty()) {
            String token = headers.get("Authorization").get(0).replace("Baerer ", "");
            String username = jwtTokenUtil.getUsernameFromToken(token);
            DAOUser user = userDAO.findByName(username);
            if (user.isGlobalAdmin()) {
                if (userDAO.findByName(proposedAdminUser.getUsername()) == null) {
                    return ResponseEntity.ok(userDetailsService.save(proposedAdminUser));
                } else {
                    return new ResponseEntity(HttpStatus.CONFLICT);
                }
            }
            return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
        }
        return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
    }

    @RequestMapping(value = "/register/globaladmin", method = RequestMethod.POST)
    public ResponseEntity<?> saveGlobalAdmin(@RequestBody UserDTO proposedAdminUser, @RequestHeader HttpHeaders headers) throws Exception {
        return ResponseEntity.ok(userDetailsService.saveGlobalAdmin(proposedAdminUser));
    }
}
